﻿Imports System.Runtime.InteropServices
Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Windows.Forms
Imports System.Threading
Imports System.Drawing
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Diagnostics.Process
Imports System.Web.AspNetHostingPermission
Imports System.Media
Imports System.Runtime.Remoting
Imports System.Runtime.Remoting.Channels
Imports System.Runtime.Remoting.Channels.Ipc

Namespace Azul
    Public Class Azul_api

        '###########################################################################
        '###########################################################################
        '######################## Azul API for developers #####################################
        '##             Cet classe vous facilite la programmation d'une application qui sera optimisé avec l'interface graphique de Azul . ##
        '##          En utilisant le c# ou le vb , vous serai faciliment capable de tourné et lancé votre propore application .                      ##
        '##         Azul Team , inc .                                                                                                                                                                                ##
        '##         Api for vb.net                                                                                                                                                                                    ## 
        '###########################################################################
        '###########################################################################
        '######################## Azul API for developers #####################################

#Region "Your_APP_INFO"
        'Informations sur votre application

        Public Const APP_name = "AzYoutube" 'Nom de votre application
        Public Const APP_icone = "" 'Icone de votre application(web link or local link)
        Public Const APP_version = "1.0.0" 'Version de votre application
        Public Const APP_maj_link = "www.azul.com" ' Maj de votre application
        Public Const APP_description = "Tool Youtube pour télécharer vos vidèos youtube ." ' Description de votre application
        Public Const APP_type = "Application" ' Le type de votre application
        Public Const APP_public_key = "" 'Key fournis sur le site de Azul developers 

        'Informations Azul_your_app
        Public Const Azul_app_menu = False ' Si votre application s'affichera dans la bare de Azul
        Public Const Azul_app_start = False 'Si votre application s'affichera dans le menu start de Azul
        Public Const Azul_app_infos_right = "AZ_cl" 'Détermine le niveau d'autorisation d'accés au fichier
        Public Const Azul_app_error = True 'Determine si les erreurs de votre applictaion f'afficheron avec la function azul_errors
        Public Const Azul_app_useconnexion = False 'Si votre application utlisera l'internet .
        Public Const Azul_app_useazserver = False 'Si votre applciation se connectera aux serveurs d'Azul .
        Public Const Azul_app_newbare = False 'Si votre app créera une nouvelle bare .
        Public Const Azul_app_capturscreen = False 'Votre application peux utiliser la function azul_capture pour capturer le screen

        'Declarer

        ' Public Shared APP_ip = az_connect.LocalIP
        Public Shared az_data As String = ""
        Public Shared SocketClient As Socket
        Public Shared LocalsocketClientIsShutingDown As Boolean
        Public Shared readbuf As Byte()
        Public Shared sendbuf As Byte()
        Public Shared listBox As ListBox

        Public Delegate Sub DelegateDisplay(ByVal message As String)
        '  Public Shared dlgDisplay As DelegateDisplay = New DelegateDisplay(AddressOf DisplayMessage)
#End Region



#Region "Azul ipc"

        Public Interface ICommunicationService

            Sub SaySomething(ByVal text As String)
        End Interface

        Public Shared Sub lunch(ByVal message)
            Try
                Dim ipcCh As New IpcChannel("AZULAPP")
                ChannelServices.RegisterChannel(ipcCh, False)

                Dim obj As SharedInterfaces.ICommunicationService = _
                  DirectCast(Activator.GetObject(GetType(SharedInterfaces.ICommunicationService), _
                   "ipc://IPChannel/SreeniRemoteObj"), SharedInterfaces.ICommunicationService)
                obj.SaySomething(message)

                ChannelServices.UnregisterChannel(ipcCh)
            Catch ex As Exception
                MsgBox("Azul n'est pas ouvert pour executé votre application" & vbCrLf & ex.ToString)
                Application.Exit()
            End Try

        End Sub
        Public Shared obj As SharedInterfaces.ICommunicationService = _
         DirectCast(Activator.GetObject(GetType(SharedInterfaces.ICommunicationService), _
         "ipc://IPChannelName/SreeniRemoteObj"), SharedInterfaces.ICommunicationService)
#End Region

#Region "Azul sockets"
        Sub New(ByVal list As System.Windows.Forms.ListBox)

            readbuf = New Byte(100) {}
        End Sub

        Public Shared Sub ConnectToServer(ByVal ServerName As String)
            Try
                If SocketClient Is Nothing OrElse Not SocketClient.Connected Then
                    SocketClient = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                    Dim ipadress As IPAddress()
                    Dim he As IPHostEntry = Dns.GetHostByName(ServerName)
                    '   ServerName = "10.63.219.252"
                    ipadress = he.AddressList
                    SocketClient.BeginConnect(New IPEndPoint(ipadress(0), 15), AddressOf ConnectCallback, SocketClient)

                Else
                    Dim obj As String() = New String() {"Déjà connecté à Azul"}
                    MsgBox("Azul n'est pas lancé pour executer votre application")
                    Application.Exit()
                End If
            Catch ex As Exception

            End Try

        End Sub

        Private Shared Sub ConnectCallback(ByVal asyncResult As IAsyncResult)
            Dim obj As String()
            Try
                Dim socket As Socket = CType(asyncResult.AsyncState, Socket)
                SocketClient = socket
                socket.EndConnect(asyncResult)
                obj = New String() {"Connecté  à Azul"}
                send_data()
                '  listBox.Invoke(dlgDisplay, obj)
                LocalsocketClientIsShutingDown = False
                SocketClient.BeginReceive(readbuf, 0, readbuf.Length, SocketFlags.None, AddressOf ReceiveCallback, SocketClient)


            Catch ex As SocketException
                obj = New String() {ex.Message}
                If ex.Message.Contains("être établie") Then
                    MsgBox("Azul n'est pas lancé pour executer votre application")
                    Application.Exit()
                End If
            End Try
        End Sub

        Public Shared Sub SendMessage(ByVal message As String)
            If Not (SocketClient Is Nothing) AndAlso SocketClient.Connected Then
                sendbuf = Encoding.ASCII.GetBytes(message)
                SocketClient.BeginSend(sendbuf, 0, sendbuf.Length, SocketFlags.None, AddressOf SendCallback, SocketClient)
            Else
                MsgBox("Non connecté à Azul.")
            End If
        End Sub

        Private Shared Sub SendCallback(ByVal asyncResult As IAsyncResult)
            Dim obj As String()
            Try
                Dim socket As Socket = CType(asyncResult.AsyncState, Socket)
                Dim send As Integer = socket.EndSend(asyncResult)
                obj = New String() {"Message envoyé (" + send.ToString() + " bytes envoyés )"}
                '    listBox.Invoke(dlgDisplay, obj)
            Catch ex As SocketException
                obj = New String() {ex.Message}

            End Try
        End Sub

        Public Shared Sub ReceiveMessage()
            If Not (SocketClient Is Nothing) AndAlso SocketClient.Connected Then
                SocketClient.BeginReceive(readbuf, 0, readbuf.Length, SocketFlags.None, AddressOf ReceiveCallback, SocketClient)
            Else
                MsgBox("Non connecté à azul.")
            End If
        End Sub

        Private Shared Sub ReceiveCallback(ByVal asyncResult As IAsyncResult)
            Dim obj As String()
            Try
                Dim socket As Socket = CType(asyncResult.AsyncState, Socket)
                Dim read As Integer = socket.EndReceive(asyncResult)
                If read > 0 Then
                    obj = New String() {"Azul dit :" + Encoding.ASCII.GetString(readbuf)}
                    '  listBox.Invoke(dlgDisplay, obj)
                    SocketClient.BeginReceive(readbuf, 0, readbuf.Length, SocketFlags.None, AddressOf ReceiveCallback, SocketClient)
                End If
                If read = 0 AndAlso Not LocalsocketClientIsShutingDown Then
                    SocketClient.Close()
                    obj = New String() {"Fermeture socket distante"}

                End If
                Buffer.SetByte(readbuf, 0, 0)
            Catch ex As SocketException
                obj = New String() {ex.Message}

            End Try
        End Sub

        Public Shared Sub Close()
            If Not (SocketClient Is Nothing) AndAlso SocketClient.Connected Then
                LocalsocketClientIsShutingDown = True
                SocketClient.Shutdown(SocketShutdown.Both)
                SocketClient.Close()
                Dim obj As String() = New String() {"Connexion fermée"}
            End If
        End Sub
#End Region

#Region "Azul_function"
        Public Shared Sub start()
            send_app_infos()
        End Sub

        Public Shared Sub send_data()
            Try
                send_app_infos()
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

        End Sub
        Public Shared Sub send_app_infos()
            Try
                lunch("APP_infos")
                lunch("name=" & APP_name)
                lunch("icone=" & APP_icone)
                lunch("version=" & APP_version)
                lunch("Type =" & APP_type)
                lunch("description=" & APP_description)
                send_azul_data()
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

        End Sub
        Public Shared Sub send_azul_data()
            Try
                lunch("Azul_infos")
                traiterdata()
                ' az_connect.SendData("Azul_app_menu=" & Azul_app_menu)
                ' az_connect.SendData("Azul_app_capturscreen=" & Azul_app_capturscreen)
                '  az_connect.SendData("Azul_app_infos_right=" & Azul_app_infos_right)
                '  az_connect.SendData("Azul_app_error=" & Azul_app_error)
                ' az_connect.SendData("Azul_app_newbare=" & Azul_app_newbare)
                ' az_connect.SendData("Azul_app_start=" & Azul_app_start)
                ' az_connect.SendData("Azul_app_useazserver=" & Azul_app_useazserver)
                ' az_connect.SendData("Azul_app_useconnexion=" & Azul_app_useconnexion)

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End Sub
        Public Shared Sub doo()

        End Sub

#End Region

#Region "Azul_data"
        Public Shared Sub traiterdata()
            If Azul_app_capturscreen = True Then
                lunch("cp_ok")
            Else
                lunch("cp_no")
            End If
            If Azul_app_error = True Then
                lunch("er_ok")
            Else
                lunch("er_no")
            End If
            If Azul_app_infos_right = "AZ_cl" Then
                lunch("ir_cl")
            Else
                lunch("ir_ad")
            End If
            If Azul_app_menu = True Then
                lunch("m_ok")
            Else
                lunch("m_no")

            End If
            If Azul_app_newbare = True Then
                lunch("nb_ok")
            Else
                lunch("nb_no")
            End If
            If Azul_app_start = True Then
                lunch("st_ok")
            Else
                lunch("st_no")
            End If
            If Azul_app_useazserver = True Then
                lunch("us_ok")

            Else
                lunch("us_no")
            End If
            If Azul_app_useconnexion = True Then
                lunch("uc_ok")
            Else
                lunch("uc_no")
            End If

        End Sub

#End Region

#Region "Azul_files"
        Public Shared Sub informations()

        End Sub
#End Region

    End Class
End Namespace
